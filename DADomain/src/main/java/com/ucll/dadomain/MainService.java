/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.dadomain;

import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author edweve0
 */

@RequestScoped
@Named
@Default
public class MainService {
        private EntityManagerFactory emfactory;
        private EntityManager entitymanager;
    
      public MainService(){
      emfactory = Persistence.createEntityManagerFactory( "CarUnit" );
      
      entitymanager = emfactory.createEntityManager();
      }
      
      public void addCar(Car car){
      entitymanager.getTransaction( ).begin( );

      entitymanager.persist( car );
      entitymanager.getTransaction( ).commit( );

      entitymanager.close( );
      emfactory.close( );
      }
        
        
      public void addCar(String model, String brand){
      entitymanager.getTransaction( ).begin( );

      Car car = new Car(); 
      car.setBrand(brand);
      car.setModel(model);
      
      entitymanager.persist( car );
      entitymanager.getTransaction( ).commit( );

      entitymanager.close( );
      emfactory.close( );
      }
         
      public Car getCar(Long id){
          Car car = entitymanager.find(Car.class, id);
          return car;
      }
      
       public Collection<Car> getAllCars() {
       Query query = entitymanager.createQuery("SELECT e FROM Car e");
       return (Collection<Car>) query.getResultList();
       }
       
       public Collection<Video> getAllVideos() {
       Query query = entitymanager.createQuery("SELECT e FROM Video e");
       return (Collection<Video>) query.getResultList();
       }
     
      
      public void addVideo(String url, Long carId){
      entitymanager.getTransaction( ).begin( );

      Video video = new Video();
      video.setUrl(url);
      video.setCarId(carId);
      
      entitymanager.persist( video );
      entitymanager.getTransaction( ).commit( );

      entitymanager.close( );
      emfactory.close( );
      }
      
  
    
    
}
