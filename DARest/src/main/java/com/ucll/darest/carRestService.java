/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.darest;

import com.ucll.dadomain.Car;
import com.ucll.dadomain.MainService;
import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author edweve0
 */
@Path("car")
@Named
@RequestScoped
public class carRestService {

    @Inject
    private MainService service;
    

    /**
     * Creates a new instance of restService
     */
    public carRestService() {
        
    }

    @GET
    @Produces("text/plain")
    public String hello() {
        return "HEllo elias";
    }
    
    @GET
    @Path("getCar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Car getCar(@PathParam("id") Long id){
        return service.getCar(id);
        
    }
    
    @GET
    @Path("getAllCars")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Car> getAllCars() {
    return service.getAllCars();
    }
    
    @POST
    @Path("addCar/{brand}/{model}")
    //@Consumes(MediaType.APPLICATION_JSON)
    public Response addCar(@PathParam("brand") String brand, @PathParam("model") String model){
        System.out.println(brand);
        service.addCar(model,brand);
        return Response.status(201).build();
        
    }


}
