/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.darest;


import com.ucll.dadomain.MainService;
import com.ucll.dadomain.Video;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author edweve0
 */
@Path("video")
@Named
@RequestScoped
public class videoRestService {

    @Inject
    private MainService service;
    

    /**
     * Creates a new instance of videoRestService
     */
    public videoRestService() {
    }
    
    
    
    @POST
    @Path("addVideo/{videoId}/{carId}")
    public Response addVideo(@PathParam("videoId") String videoId, @PathParam("carId") Long carId){
        System.out.println(videoId);
        service.addVideo(videoId,carId);
        return Response.status(201).build();
        
    }
    
    @GET
    @Path("getAllVideos")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Video> getAllVideos() {
    return service.getAllVideos();
    }
    
    @GET
    @Path("getYoutubeVideos/{subject}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getYoutubeVideos(@PathParam("subject") String subject) throws IOException, JSONException {
        String url = "https://www.googleapis.com/youtube/v3/search?q="+subject+"&part=snippet&key=AIzaSyAf7T3vUaFkP9JAQn79RAqCx2tdhIwiwSQ&type=video&maxResults=10";
        List<String> resultList = new ArrayList<>();
    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONObject json = new JSONObject(jsonText);
        JSONArray items = json.getJSONArray("items");
        for (int i = 0; i<items.length(); i++){
            JSONObject obj = items.getJSONObject(i);
            JSONObject idObj = obj.getJSONObject("id");
            System.out.println(idObj.toString());
            resultList.add(idObj.getString("videoId")) ;
        }
           System.out.println(resultList.toString());
           return resultList;
    } finally {
      is.close();
    }
  }
    



    
    private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }
}
